<?php

/**
 * This file should be used to render each module instance.
 * You have access to two variables in this file:
 *
 * $module An instance of your module class.
 * $settings The module's settings.
 *
 */
    // Get the map pins
    $communities = $module->get_markers($settings);
    $markers = '';
    $comm_count = count($communities);
    $i = 0;
?>
<?php foreach ($communities as $community):
	if($community['coordinates'] != null || $community['coordinates'] != ","):
        $coords = $community['coordinates'];
        $ltlng = explode(',' , $coords );
        $markers .= '[\'<h3><a href="'.$community['link'].'" target="_BLANK">';
        $markers .= $community['title']['rendered'] . "</a></h3>','";
//        $markers .= json_encode($community['content']['rendered']) ."','";
        $markers .= $ltlng[0] . "','";
        $markers .= $ltlng[1] . "']";

        if ($i != $comm_count -1){
            $markers .= ',';
        }
    endif;
    $i++;
    endforeach;
?>

<div class="lam-community-map">
    <script>
        function initMap(){
            var map;
            var bounds = new google.maps.LatLngBounds();
            var mapOptions = {
                mapTypeId: 'roadmap'
            };

            // Display a map on the page
            map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
            map.setTilt(45);

            var markers = [<?php echo $markers ?>]; //Locations array closing tag
            var infowindow = new google.maps.InfoWindow();
            var marker, i;

            for (i = 0; i < markers.length; i++) {
                var position = new google.maps.LatLng(markers[i][1],markers[i][2]);
                bounds.extend(position);
                marker = new google.maps.Marker({
                    position: position,
                    map: map,
                    title: markers[i][0]
                });
                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        infowindow.setContent(markers[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));

                // Automatically center the map fitting all markers on the screen
                map.fitBounds(bounds);
            }
        }

//        debugger;
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBWe0XvFoSuXrEx69V6IogOg2v-9Z6C4ZQ&callback=initMap"></script>
    <div id="map_canvas"></div>
</div>

