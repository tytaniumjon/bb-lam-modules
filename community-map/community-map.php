<?php

/**
 * This is an lam-region-listing module with only the basic
 * setup necessary to get it working.
 *
 * @class LamCommunityMapModule
 */
class LamCommunitiesMapModule extends FLBuilderModule {

    /** 
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */  
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Community Map', 'fl-builder'),
            'description'   => __('An basic lam-region-listing for coding new modules.', 'fl-builder'),
            'category'		=> __('Laguna Asset Modules', 'fl-builder'),
            'dir'           => LAM_MODULES_DIR . 'community-map/',
            'url'           => LAM_MODULES_URL . 'community-map/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));
    }

    public function get_markers($settings){

	    $post_type_slug = $settings->communities_post_type;
		$json_url = $settings->rest_root . '/wp-json/wp/v2/'. $post_type_slug .'?per_page=100';
		$json_resp = wp_remote_get($json_url);
		$response = json_decode(wp_remote_retrieve_body($json_resp),true);
		return $response;
    }

	public function geocode($address){
		// url encode the address
		$address = urlencode($address);

		// google map geocode api url
		$url = "http://maps.google.com/maps/api/geocode/json?address={$address}";

		// get the json response
		$resp_json = file_get_contents($url);

		// decode the json
		$resp = json_decode($resp_json, true);

		// response status will be 'OK', if able to geocode given address
		if($resp['status']=='OK'){
			// get the important data
			$lati = $resp['results'][0]['geometry']['location']['lat'];
			$longi = $resp['results'][0]['geometry']['location']['lng'];
			$formatted_address = $resp['results'][0]['formatted_address'];

			// verify if data is complete
			if($lati && $longi && $formatted_address){

				// put the data in the array
				$data_arr = array();

				array_push(
					$data_arr,
					$lati,
					$longi,
					$formatted_address
				);

				return $data_arr;

			}else{
				return $resp;
			}
		}else{
			return;
		}
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('LamCommunitiesMapModule', array(
    'general'       => array( // Tab
        'title'         => __('General', 'fl-builder'), // Tab title
        'sections'      => array( // Tab Sections
            'general'       => array( // Section
                'title'         => __('Section Title', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'rest_root'     => array(
                        'type'          => 'text',
                        'label'         => __('REST Root', 'fl-builder'),
                        'default'       => get_bloginfo('wpurl'),
                        'preview'         => array(
                            'type'             => 'css',
                            'selector'         => '.fl-lam-region-listing-text',
                            'property'         => 'font-size',
                            'unit'             => 'px'
                        )
                    ),
                    'communities_post_type' => array(
                        'type' => 'post-type',
	                    'label' => 'Community Post Type',
                    ),
                    'community_map_loop' => array(
                    	'title' => 'Loop Settings',
	                    'file' => FL_BUILDER_DIR . '/includes/loop-settings.php'
                    ),
                    'color_field'    => array(
                        'type'          => 'color',
                        'label'         => __('Color Picker', 'fl-builder'),
                        'default'       => '333333',
                        'show_reset'    => true,
                        'preview'         => array(
                            'type'            => 'css',
                            'selector'        => '.fl-lam-region-listing-text',
                            'property'        => 'color'
                        )
                    ),
                )
            )
        )
    )
));