<?php
/**
 * Plugin Name: Laguna Asset Management/Communities Beaver Builder Modules
 * Plugin URI: https://lagunaassetcommunities.com/
 * Description: Custom modules built by Tytanium Ideas for Laguna Asset Management & Communities.
 * Version: 1.0.1
 * Author: The Tytanium Ideas
 * Author URI: https://tytaniumideas.com
 */
define( 'LAM_MODULES_DIR', plugin_dir_path( __FILE__ ) );
define( 'LAM_MODULES_URL', plugins_url( '/', __FILE__ ) );

/**
 * Custom modules
 */
function lam_load_modules() {
	if ( class_exists( 'FLBuilder' ) ) {
	    require_once 'community-map/community-map.php';
	    //require_once 'lam-region-listing/lam-community-region.php';
	}
}
add_action( 'init', 'lam_load_modules' );
